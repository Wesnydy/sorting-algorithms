#include <stdlib.h>
#include <time.h>
#include <algorithm>

#include "util.hpp"
#include "bubblesort.hpp"
#include "insertionsort.hpp"
#include "selectionsort.hpp"
#include "mergesort.hpp"
#include "quicksort.hpp"
#include "heapsort.hpp"

using namespace std;

int main(int argc, char **argv) {
  int algorithm = -1;
  string file = "";

  read_args(argv, argc, file, algorithm);

  if (algorithm == -1 || file == "") {
    clog << "not enough parameters" << endl;
    return 1;
  }

  ifstream ifs_;
  ifs_.open(file, ifstream::in);

  if ( ! (ifs_.is_open() && ifs_.good())) {
    clog << "Can't open file" << endl;
    return 1;
  }

  vector<int> *vec = new vector<int>();

  char *ptr;
  int element;
  string current_line;
  while (ifs_.good()) {
    getline(ifs_, current_line);
    element = strtol(current_line.c_str(), &ptr, 10);
    vec->push_back(element);
  }

  switch (algorithm) {
    case _BUBBLE_:
      {
        std::clog << "Running Bubblesort on input " << file << std::endl;
        Bubblesort *bubble = new Bubblesort();

        clock_t tStart = clock();
        bubble->sort_vector(*vec);
        double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

        printf("Time elapsed: %.2fsec\n\n", etime);
        write_log("bubble", file, *vec);

        delete bubble;
        break;
      }

    case _INSERTION_:
      {
        std::clog << "Running InsertionSort on input " << file << std::endl;
        InsertionSort *insertion = new InsertionSort();

        clock_t tStart = clock();
        insertion->sort_vector(*vec);
        double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

        printf("Time elapsed: %.2fsec\n\n", etime);
        write_log("insertion", file, *vec);

        delete insertion;
        break;
      }

    case _SELECTION_:
      {
        std::clog << "Running SelectionSort on input " << file << std::endl;
        SelectionSort *selection = new SelectionSort();

        clock_t tStart = clock();
        selection->sort_vector(*vec);
        double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

        printf("Time elapsed: %.2fsec\n\n", etime);
        write_log("selection", file, *vec);

        delete selection;
        break;
      }

    case _MERGE_:
      {
        std::clog << "Running MergeSort on input " << file << std::endl;
        MergeSort *merge = new MergeSort();

        clock_t tStart = clock();
        merge->sort_vector(*vec);
        double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

        printf("Time elapsed: %.4fsec\n\n", etime);
        write_log("merge", file, *vec);

        delete merge;
        break;
      }

    case _QUICK_:
      {
        std::clog << "Running QuickSort on input " << file << std::endl;
        QuickSort *quick = new QuickSort();

        clock_t tStart = clock();
        quick->sort_vector(*vec);
        double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

        printf("Time elapsed: %.4fsec\n\n", etime);
        write_log("quick", file, *vec);

        delete quick;
        break;
      }

    case _HEAP_:
      {
        std::clog << "Running HeapSort on input " << file << std::endl;
        HeapSort *heap = new HeapSort();

        clock_t tStart = clock();
        heap->sort_vector(*vec);
        double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

        printf("Time elapsed: %.4fsec\n\n", etime);
        write_log("heap", file, *vec);

        delete heap;
        break;
      }

    case _CPPSORT_:
    {
      std::clog << "Running C++ sort on input " << file << std::endl;

      clock_t tStart = clock();
      std::sort(vec->begin(), vec->end());
      double etime = (double)(clock() - tStart)/CLOCKS_PER_SEC;

      printf("Time elapsed: %.4fsec\n\n", etime);
      write_log("cppsort", file, *vec);
      break;
    }

    default:
      std::clog << "Invalid Option" << std::endl;
      delete vec;
      return 1;
  }

  delete vec;

  return 0;
}
