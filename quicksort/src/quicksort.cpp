#include "quicksort.hpp"

void QuickSort::sort_vector(vector<int> &vec) {
  int begin = 0;
  int end = vec.size()-1;

  quicksort(vec, begin, end);
}

void QuickSort::quicksort(vector<int> &vec, int begin, int end) {
  if (begin < end) {
    int middle = partition(vec, begin, end);
    quicksort(vec, begin, middle);
    quicksort(vec, middle+1, end);
  }
}

int QuickSort::partition(vector<int> &vec, int begin, int end) {
  int top = begin;
  int pivo = vec[begin];

  for (int i = begin+1; i <= end; i++) {
    if (vec[i] < pivo) {
      vec[top] = vec[i];
      vec[i] = vec[top+1];
      top++;
    }
  }
  vec[top] = pivo;
  return top;
}
