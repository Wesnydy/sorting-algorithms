#ifndef QUICKSORT_H_
#define QUICKSORT_H_

#include <vector>

using namespace std;

class QuickSort {
 public:
   QuickSort() {};
   ~QuickSort() {};

   void sort_vector(vector<int> &vec);

 private:
   int partition(vector<int> &vec, int begin, int end);
   void quicksort(vector<int> &vec, int begin, int end);
};

#endif // QUICKSORT_H_
