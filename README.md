# Algoritmos de ordenação #

Implementação de alguns algoritmos utilizados para a ordenação de elementos numéricos.

### Algoritmos implementados ###

* Bubble Sort
* Insertion Sort
* Selection Sort
* Merge Sort
* Quick Sort
* Heap Sort

* [Animações dos algoritmos de ordenação](https://www.toptal.com/developers/sorting-algorithms)

### Compilando o projeto ###

 Para compilar o projeto, basta digitar o seguinte comando no terminal:
```sh
    $ make
```

### Executando os testes ###

 Para executar os testes, garanta que seus arquivos estão na pasta /test 
 e então execute o script de testes com o seguinte comando:
```sh
    $ ./exec.sh
```

### Contribuidores ###

* Wesnydy Ribeiro <wesnydy1@gmail.com>