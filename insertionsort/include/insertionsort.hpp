#ifndef INSERTIONSORT_H_
#define INSERTIONSORT_H_

#include <vector>

using namespace std;

class InsertionSort {
 public:
   InsertionSort() {};
   ~InsertionSort() {};

   void sort_vector(vector<int> &vec);
};

#endif // INSERTIONSORT_H_
