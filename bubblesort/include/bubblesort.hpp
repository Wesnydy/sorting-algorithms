#ifndef BUBBLESORT_H_
#define BUBBLESORT_H_

#include <vector>

using namespace std;

class Bubblesort {
 public:
   Bubblesort() {};
   ~Bubblesort() {};

   void sort_vector(vector<int> &vec);
};

#endif // BUBBLESORT_H_
