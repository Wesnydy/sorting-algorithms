#!/bin/sh

echo "\nRunning tests, please wait ... \n"

BIN=sorting

for i in test/*."in"; do
  ./$BIN -F $i --insertion;
  ./$BIN -F $i --selection;
  ./$BIN -F $i --merge;
  ./$BIN -F $i --quick;
  ./$BIN -F $i --heap;
  ./$BIN -F $i --cppsort;
  echo "----------------------------------------\n"
done

echo "Tests finalized! \n"
