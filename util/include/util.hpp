#ifndef UTIL_H_
#define UTIL_H_

#include <getopt.h>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#define _BUBBLE_ 0
#define _SELECTION_ 1
#define _INSERTION_ 2
#define _MERGE_ 3
#define _QUICK_ 4
#define _HEAP_ 5
#define _CPPSORT_ 6

#define _OUT_EXT_ ".out"

using namespace std;

void read_args(char **argv, int argc, string &file, int &algorithm) {
  int opt;
  int long_index = 0;
  static struct option long_options[] = {
    {"file", required_argument, NULL, 'F'},
    {"bubble",     no_argument, NULL, 'b'},
    {"selection",  no_argument, NULL, 's'},
    {"insertion",  no_argument, NULL, 'i'},
    {"merge",      no_argument, NULL, 'm'},
    {"quick",      no_argument, NULL, 'q'},
    {"heap",       no_argument, NULL, 'h'},
    {"cppsort",    no_argument, NULL, 'c'},
    {0, 0, 0, 0}
  };

  while ((opt = getopt_long_only(argc, argv, "F:b:s:i:m:q:h:c:", long_options, &long_index))!= -1) {
    switch (opt) {
      case 'F':
        file = optarg;
        break;

      case 'b':
        algorithm = _BUBBLE_;
        break;

      case 's':
        algorithm = _SELECTION_;
        break;

      case 'i':
        algorithm = _INSERTION_;
        break;

      case 'm':
        algorithm = _MERGE_;
        break;

      case 'q':
        algorithm = _QUICK_;
        break;

      case 'h':
        algorithm = _HEAP_;
        break;

      case 'c':
        algorithm = _CPPSORT_;
        break;

      case '?':
        // O getopt já mostra o erro no terminal
        break;
    }
  }
}

void print_vector(const std::vector<int> &vec) {
  std::clog << "[ ";
  for (std::vector<int>::const_iterator i = vec.begin(); i != vec.end(); i++) {
    std::clog << *i << " ";
  }
  std::clog << "]\n" << std::endl;
}

void write_log(const std::string &algorithm, const std::string &file, const std::vector<int> &vec) {
  std::string outfile = file;
  outfile.append(".").append(algorithm)
  .append(_OUT_EXT_);

  std::ofstream out;
  out.open(outfile, std::ios::app);
  for (std::vector<int>::const_iterator i = vec.begin(); i != vec.end(); i++) {
    out << *i << "\n";
  }

}

#endif // UTIL_H_
